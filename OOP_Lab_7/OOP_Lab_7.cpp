﻿//Напишите программу, которая запрашивает температуру в градусах по Цельсию и отображает ее эквивалент по Фарингейту
#include "pch.h"
#include <iostream>
using namespace std;
int main()
{
	setlocale(0, "");
	float celsius;
	cout << "Введите значение градусов по Цельсию: ";
	cin >> celsius;
	cout << celsius << " градусов по Цельсию = " << (celsius * 9 / 5) + 32 << " градусов по Фарингейту ";
	system("pause");
	return 0;
}